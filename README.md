# ![LARA-logo](docs/images/lara/LARA_logo.svg) LARAsuite

**LARA** (Laboratory Automation Robotic Assistent) is an open source **Research Data Management Suite** and 2nd generation Electronic Lab Notebook with the goal to automate most of the data and metadata entering process by providing a rich infrastructure for lab communication, orchestration, data/metadata storage, evaluation and exchange.
It utilises [Ontology](https://gitlab.com/opensourcelab/scientificdata/ontologies) based semantic web technology to make data findable [via SPARAQL](https://w3c.org/sparql).

**LARA** is designed to reduce manual data entry by humans to the bare minimum, since **scientists should fokus on the creative part of their research** ("no one likes to enter data by hand, if a machine can do it").
This is achieved by a very fine grained, [gRPC](https://grpc.io/) based API and a semantic, ontology based representation of most items.

Please note, that **LARA** is still in a vivid development stage, so please be not disappointed, if there are "hickups" or "gaps". We are very happy for any feeback and proposals via [LARA gitlab issues](https://gitlab.com/groups/LARAsuite/-/issues), it should be a community project, so please feel free to contribute !! **Thank you very much in advance !!**


## Architecture

The LARA architecture is based on a microservice architecture. The following diagram shows the current architecture of LARA:

![LARA-architecture](docs/images/lara/LARA_buttom_up_workflow_full.svg)


### [Planning](https://gitlab.com/LARAsuite/lara-django-projects)

Scientific research is structured in **LARA** in [Projects](https://gitlab.com/LARAsuite/lara-django-projects) and [Experiments](https://gitlab.com/LARAsuite/lara-django-projects):

[Projects](https://gitlab.com/LARAsuite/lara-django-projects) can have as many sub-experiments as desired, similarily, [Experiments](https://gitlab.com/LARAsuite/lara-django-projects) can have many *sub-experiments*.


### [Process](https://gitlab.com/LARAsuite/lara-django-processes) [Orchestration](https://gitlab.com/opensourcelab/laborchestrator) / [Scheduling](https://gitlab.com/opensourcelab/pythonlabscheduler)

Experiments can be structured in [Processes](https://gitlab.com/LARAsuite/lara-django-processes) and [Procedures](https://gitlab.com/LARAsuite/lara-django-processes).
[Processes](https://gitlab.com/LARAsuite/lara-django-processes) describe "what" is done in an experiment, while [Procedures](https://gitlab.com/LARAsuite/lara-django-processes) describe "how" it is done.

[Processes](https://gitlab.com/LARAsuite/lara-django-processes) and [Procedures](https://gitlab.com/LARAsuite/lara-django-processes) can be denoted in the [pythonLab Procdure/Process description language](https://gitlab.com/opensourcelab/pythonLab), 
a very powerful, yet simple language, even suited for very complex procdures and processes, including loops, conditions, etc., enabling closed loop automation / experiments.


These [Processes](https://gitlab.com/LARAsuite/lara-django-processes) and [Procedures](https://gitlab.com/LARAsuite/lara-django-processes) are then executed by the [Lab Orchestrator](https://gitlab.com/opensourcelab/laborchestrator) and [pythonlab scheduler](https://gitlab.com/opensourcelab/pythonlabscheduler), which communicate with the corresponding [SiLA servers](https://sila-standard.com/), like [instruments, reactors, robots](https://gitlab.com/opensourcelab/devices), machine learning algorithms or even humans (human feedback is informed/integrated via an app).


The [Laborchestrator](https://gitlab.com/opensourcelab/laborchestrator) is a microservice that receives messages from the [LARA Django API](https://gitlab.com/LARAsuite/lara-django) to orchestrate the execution of experiments and procedures on the corresponding SiLA servers. The [pythonlab scheduler](https://gitlab.com/opensourcelab/pythonlabscheduler) microservice calculates the best schedule in which order the process and procedure steps should be executed.


### [Data Collection](https://gitlab.com/LARAsuite/lara-django-data-collector)

The [Data Collector](https://gitlab.com/LARAsuite/lara-django-data-collector) is a microservice that receives messages from the [Laborchestrator](https://gitlab.com/opensourcelab/laborchestrator) to collect data from [corresponding SiLA servers](https://gitlab.com/opensourcelab/devices), if data is ready for collection and stores the data in the corresponding [LARA data database](https://gitlab.com/LARAsuite/lara-django-data). Larger datasets are not directly stored in the database, but in an S3-compliant object storage, [minio](https://min.io/) by default, and only linked to the entries in the database. We recoommend to store the data in an efficient, open data format, like [SciDat](https://gitlab.com/opensourcelab/scientificdata/scidat), which combines tabular data with (semantic, [JSON-LD](https://json-ld.org/) based) metadata in a single, compact file.

### [LARA database](https://gitlab.com/LARAsuite/lara-django)

The [LARA database](https://gitlab.com/LARAsuite/lara-django) is [python django](https://www.djangoproject.com/) based and consist of many modules (the current set can be [easily expanded](https://gitlab.com/LARAsuite/lara-django-app-cookiecutter) by specific applications): 

Currently we have modules for [Procdures, Processes and Methods](https://gitlab.com/LARAsuite/lara-django-processes), [Material - Parts, Devices and Labware](https://gitlab.com/LARAsuite/lara-django-material),  [Substances, Polymers, Mixtures and Reactions](https://gitlab.com/LARAsuite/lara-django-substances), [Sequences, like DNA, RNA, peptide/protein sequences](https://gitlab.com/LARAsuite/lara-django-sequences), [3D Structures of molecules](https://gitlab.com/LARAsuite/lara-django-structure), [Organisms](https://gitlab.com/LARAsuite/lara-django-organisms), [Samples](https://gitlab.com/LARAsuite/lara-django-samples), [Data](https://gitlab.com/LARAsuite/lara-django-data) and for the management of [Scientists, Institutions, Companies, etc.](https://gitlab.com/LARAsuite/lara-django-people).

**LARA** differentiates between "abstract" entities, like "the substance ethanol in general with its generic properties molecular weight and sum formula etc." or "thermometer" as a "generic thermometer as a device for measureing temperature" and an "instance" of these abstracta: "the bottle of ethanol in a particular lab from a particular vendor" / "the thermometer of vendor x with a temperature range 250-350K".

These instance information of individual objects is stored in the corresponding stores. Currently,

[Substance Store](https://gitlab.com/LARAsuite/lara-django-substances-store), [Material Store](https://gitlab.com/LARAsuite/lara-django-material-store) and [Organism Store](https://gitlab.com/LARAsuite/lara-django-organisms-store).

These stores can also be used as **"free" inventories** for the lab (ordering workflows will follow soon).

The *"class/abstractum"* versus *"instance/individuum"* paradigm is a common design paradigm in **LARA**.

### [Semantic Database](https://gitlab.com/LARAsuite/lara-django)

For the semantic representation of the items a triple store is used, which is based on [openlink virtuoso](https://virtuoso.openlinksw.com/). The terms are defined in [Ontologies](https://gitlab.com/opensourcelab/scientificdata/ontologies). This semantic representation is used to make the data findable via [SPARQL](https://w3c.org/sparql).
The semantic information generated automatically, when new database items are created.

### [Data(-base) Synchronisation](https://gitlab.com/LARAsuite/lara-django-data-sync)

Selected parts of projects, experiments and data can be synchronised with other LARA instances via the [Data Synchronisation](https://gitlab.com/LARAsuite/lara-django-data-sync) microservice.

### [Data Evaluation](https://gitlab.com/LARAsuite/lara-django-data-evaluator)

Data can be evaluated via the [Data Evaluation](https://gitlab.com/LARAsuite/lara-django-data-evaluator) microservice, which is based on [pandas](https://pandas.pydata.org/), [numpy](https://numpy.org/) and [scipy](https://www.scipy.org/).

The evalated data is then visualised via default [Data Visualisations](https://gitlab.com/LARAsuite/lara-django-data).

For more advanced evaluations and visualisations, [Jupyter Notebooks](https://jupyter.org/) can be used, as all data is accessible via the [LARA gRPC APIs](https://gitlab.com/LARAsuite/lara-django).

### [Search](https://gitlab.com/LARAsuite/lara-django)

For simple searches, each Module has a search function, which can be used to search for specific items.
For more advanced, complex searches, the [LARA SPARQL Endpoint](https://gitlab.com/LARAsuite/lara-django) can be used.


## [Installation](https://gitlab.com/LARAsuite/lara-django/-/blob/master/README.md?ref_type=heads)

Installation is made as easy as possible via only two commands. The recommended installation method for testing is currently via [docker compose](https://docs.docker.com/engine/install/) as described in the [README.md of the lara-django repository](https://gitlab.com/LARAsuite/lara-django). [docker compose](https://docs.docker.com/engine/install/) is now part of a recent docker installations, so no additional installation is required.

## Related Projects

More generic Modules can be found at our [OpenSourceLab Project](https://gitlab.com/opensourcelab).