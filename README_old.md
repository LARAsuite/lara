# ![LARA-logo](docs/images/lara/LARA_logo.svg) LARA

The LARAsuite is a freely and openly available collection of applications, libraries, databases and tools to plan, manage, create, execute, monitor and evaluate (esp. automated) processes in the laboratory. The applications try to be as generic
as possible to cover as much as possible of classical natural science exploration, modelling a digital representation of common scientific operations.
It is designed to handle large data with Design-of-Experiments, AI and
machinelearning applications in mind. It tries to follow the .
To achieve this high goal, a broad set of modules have been created that depict many aspects of scientific work.
The vision is to cover all steps of laboratory work in a uniform framework with standardized communication protocols and data formats (like SiLA2, AnIML).

One very strong asspect in science is *reproducability*, *transparency* and *accountability* as well as  *Findability*,  *Accessibility*,  *Interoperability* and *Reusability* as expressed in the [FAIR principles](https://doi.org/10.1038%2Fsdata.2016.18) ([s. also GoFair](https://www.go-fair.org/fair-principles/) ).

One needs to be able to trust what some else did and it is important to exactly understand all the steps leading to a certain result.
This is the core of **science**.
 
Key ethos and paradigms of the **LARA suite**:
  * alwasys try to root the information to the original sources and it recommends that scientists using LARAsuite do the same
  *  every contribution that is added to the **LARA database** should be done with best knowledge and based on *Good Laboratory Practise* 
  * all data shall be semantically annotated with rich metadata, following the herin defined **Ontologies** to enable powerful **Semantic Web** technologies, like inter-linking data or do complex queries to find or aggregate data for a specific purpose, both for humans and machines. The annoation happens automatically when the data is generated.
  * direct accoutabilty and addressebilty of each scientist is very important, therefore scientists are no abstract entry in the **LARA database**, but all are part of the scientific community; their present and past affilitions shall be preserved for tracing back; each scientist receives a unique Identifier and data, she or he produced is directly related to her or him; literature and publications use the same base of information about the scientists as the projects and experiments
  * physical constants should be as accurate as the latest measurements, these are the foundaments of scientific knowledge 
  * consistancy of units: all data stored in the LARA databases should have standardised units (based on the international System of Units (SI) according to [EU legislation](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:01980L0181-20090527) and [US legislation](https://www.nist.gov/pml/weights-and-measures/metric-si/si-units) ) - this shall prevent unit-related data exchange errors,
  * measurements always come with an uncertainty. This uncertainty is depending on the particular measurement. Measured values shall always be stored with an uncertainty / error

**All (generic) tools for the lab-automation (SiLA servers, server manager), orchestration/scheduling and data reading/processing are developed in the [opensourcelab repositories](https://gitlab.com/opensourcelab)**


The LARA repository contains the  **LARA suite core specification** and ontologies, the lara-django repositories contain the python-django implementation of the LARA specification.


## Synchronisation of data between different laboratories to exchange data.

For the synchronisation of data between different laboratories, all database entries get a UUID, uniquely identifying each dataset and preventing key conflicts.

**The LARA database** tries to relate and link data as much as possible, applying [Semantic Web](https://en.wikipedia.org/wiki/Semantic_Web) technologies (*ontogies*, *OWL*, *RDF*, *SPARQL*). Furthermore all data is (automatically) enriched with [ontology-based]() metadata. This shall enable modern **Machine Learing / AI** tools to access the **LARA database** in a machine comprehensable and structured way, supplying these algorithms with as much *meta data* about measurements and their relations as possible. 


## Long term vision

Systems like the **LARAsuite** shall in the long term *replace* the old tradition of article based publication practice.

## The LARA workflow

The modules in the LARAsuite try cover all aspects of a common laboratory workflow, starting from the planning of the experiments until the final presentation of the results.

![lara-workflow](docs/images/lara/LARA_workflow.svg)
   

## The LARA structure

The LARAsuite is very mudular, enabling a flexible extension of the concept.
The modules are shown in the followoing graph: 

![lara-structure](docs/images/lara/LARA_structure.svg)


## The LARAsuite architecture

The current architecture of **LARA** has multiple layers as shown in the following scheme: 
It is based on a relational database system (e.g. postgreSQL), modules, written with [python-django](djangoproject.com) access the database through the *django ORM*. A gRPC server connects to these modules and can be used to execute remote procedure calls of a modern web client (e.g. written with [vue.js](http://vuejs.org) or similar javascript frameworks).

![LARA-architecture](docs/images/lara/LARA_application_architecture.svg)

## Implementation

A *reference implementation* of **LARA suite** is [lara-django](https://larasuite.gitlab.io/lara-django).

**LARA-django** is based on the the famous [python-django framework](https://www.djangoproject.com/).
It descriminates between abstract/general items (class) and their individual representation (instance).  

## Components
### ![Projects](docs/images/lara/LARA_projects_icon.png) [Projects](https://larasuite.gitlab.io/lara-django-projects)

All labwork can be broken down into Projects and Experiments. The Projects module supports in creating and managing these projects.

### ![Processes](docs/images/lara/LARA_process_icon.png) [Processes](https://larasuite.gitlab.io/lara-django-processes)

All processes can be created, managed and stored in the process module.

### ![Data](docs/images/lara/LARA_data_icon.png)  [Data](https://larasuite.gitlab.io/lara-django-data)

The raw and processed data of experiments and calculations can be stored in this module

### ![Substances](docs/images/lara/LARA_substances_icon.png) [Substances](https://larasuite.gitlab.io/lara-django-substances)

Chemical substance information is stored and managed here

### ![Sequences](docs/images/lara/LARA_substances_icon.png) [Sequences](https://larasuite.gitlab.io/lara-django-suquences)

Sequence information, like RNA, DNA or peptide/protein sequences can be stored and managed here

### ![Structures](docs/images/lara/LARA_substances_icon.png) [Structures](https://larasuite.gitlab.io/lara-django-structures)

3D structure information, like structures of small molecules and complexes, RNA, DNA or peptide/protein structures can be stored and managed here

### ![Organisms](docs/images/lara/LARA_substances_icon.png) [Organisms](https://larasuite.gitlab.io/lara-django-organisms)

Organism information is stored and managed here

### ![Containers](docs/images/lara/LARA_container_icon.png) [Containers](https://larasuite.gitlab.io/lara-django-containers)

The container management module

### ![Devices](docs/images/lara/LARA_devices_icon.png) [Devices](https://larasuite.gitlab.io/lara-django-devices)

Parts and Device Manager

### ![LabStore](docs/images/lara/LARA_labstore_icon.png) [LabStore](https://larasuite.gitlab.io/lara-django-substances-store)

Keeping track of Substances and Parts

Ordering new things, when empty / used ...

### ![LaraUser](docs/images/lara/LARA_people_icon.png) [User Management](https://larasuite.gitlab.io/lara-django-people)

Managing all related users

### Library

Literature, Webpage and URL collections

### Calendar

Calendar for Users, Groups for Project / Experiment / Process planning and Device booking
