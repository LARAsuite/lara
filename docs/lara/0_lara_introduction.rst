Introduction to the LARAsuite
=============================

What is the **LARAsuite** ?

The **LARAsuite** is a freely and openly available collection of applications, libraries, databases and tools to plan, manage, create, monitor and evaluate (automated) processes in the laboratory.

The vision is to cover all steps of laboratory work in a uniform framework with standardized communication protocols and data formats (like SiLA2, AnIML).

One very strong asspect in science is reproducability, transparency and accountability.

One needs to be able to trust what some other did and it is important to exactly understand all the steps leading to a certain result.
This is in the core of science.

 
![SI](docs/images/International_System_of_Units_Logo_icon.png)

These are:

  dimension  | unit | unit acronym    |     
  ---------- | ----- | --------------
  Length    | meter  |  m
  Time      | second |  s 
  Amount of substance | mol | mol
  Electric current | ampere | A
  Temperature | kelvin | K
  Luminous intensity | candela | cd
  Mass      | kilogram | kg



The LARA workflow
------------------

LARA tries cover all aspects of a common laboratory workflow, starting from the planning of the experiments until the final presentation of the results.

.. image:: ./images/lara/LARA_workflow.png
           :width: 400
           :alt: lara-workflow


The LARA structure
-------------------

The LARAsuite is very mudular, enabling a flexible extension of the concept.
The modules are shown in the followoing graph: 

.. image:: ./images/lara/LARA_structure.svg
           :width: 400
           :alt: lara-structure

The LARAsuite architecture
---------------------------

The current architecture looks like:

.. image:: images/lara/LARA_application_architecture.svg
           :width: 400
           :alt: lara-architecture
