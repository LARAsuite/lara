.. lara documentation master file, created by
   sphinx-quickstart on Tue Mar 24 11:30:41 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to lara's documentation!
================================


What is the **LARAsuite** ?
----------------------------

.. include:: README


To read more, look at the  :doc:`lara/0_lara_introduction`


.. toctree::
   :glob:
   :maxdepth: 2
   :caption: Contents:
   :titlesonly:

   lara/*

   todo
   changelog



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
