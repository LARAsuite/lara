Manual Installation of LARA suite
==================================

create venv 

activate and install 

django django-mptt gunicorn psycopg2 numpy pandas scipy grpcio grpcio-tools

nginx 
-----

/etc/nginx/sites-available/lara_django


server {
    listen 8088;
    server_name laclust1.biochemie.uni-greifswald.de;

   location = /favicon.ico { access_log off; log_not_found off; }
   location /static/ {
        root /mnt/lc1dat/data/source/projects/LARAsuite1.0;
   }

  location / {
        include proxy_params;
        proxy_pass http://unix:/run/gunicorn.sock;
  }
}



gunicorn
---------

automatic start with systemd:

/etc/systemd/system/gunicorn.service
----------------------------

[Unit]
Description=gunicorn daemon
Requires=gunicorn.socket
After=network.target

[Service]
User=lara
Group=www-data
WorkingDirectory=/mnt/lc1dat/data/source/examples/python/django/django_nginx_test
ExecStart=/home/lara/pyvenv/lara_venv/bin/gunicorn \
          --access-logfile - \
          --workers 3 \
          --bind unix:/run/gunicorn.sock \
          django_nginx_test.wsgi:application

[Install]
WantedBy=multi-user.target

-----------------------

/etc/systemd/system/gunicorn.socket

-----------------------

[Unit]
Description=gunicorn socket

[Socket]
ListenStream=/run/gunicorn.sock

[Install]
WantedBy=sockets.target

------------------------