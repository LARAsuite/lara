|LARA-logo| LARA
================

The LARAsuite is a freely and openly available collection of
applications, libraries, databases and tools to plan, manage, create,
monitor and evaluate (automated) processes in the laboratory.

The vision is to cover all steps of laboratory work in a uniform
framework with standardized communication protocols and data formats
(like SiLA2, AnIML).

One very strong asspect in science is *reproducability*, *transparency*
and *accountability*.

One needs to be able to trust what some else did and it is important to
exactly understand all the steps leading to a certain result. This is
the core of **science**.

To achieve this high goal, a broad set of modules have been created that
depict many aspects of scientific work.

Key ethos and paradigms of the **LARA suite**: \* alwasys try to root
the information to the original sources and it recommends that
scientists using LARAsuite do the same, \* physical constants should be
as accurate as the latest measurements, these are the foundaments of
scientific knowledge \* consistancy of units: all data stored in the
LARA databases should have standardised units (based on the
international System of Units (SI) according to `EU
legislation <https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:01980L0181-20090527>`__
and `US
legislation <https://www.nist.gov/pml/weights-and-measures/metric-si/si-units>`__
)

.. figure:: docs/images/International_System_of_Units_Logo_icon.png
   :alt: SI

   SI

These are:

+-----------------------+------------+----------------+
| dimension             | unit       | unit acronym   |
+=======================+============+================+
| Length                | meter      | m              |
+-----------------------+------------+----------------+
| Time                  | second     | s              |
+-----------------------+------------+----------------+
| Amount of substance   | mol        | mol            |
+-----------------------+------------+----------------+
| Electric current      | ampere     | A              |
+-----------------------+------------+----------------+
| Temperature           | kelvin     | K              |
+-----------------------+------------+----------------+
| Luminous intensity    | candela    | cd             |
+-----------------------+------------+----------------+
| Mass                  | kilogram   | kg             |
+-----------------------+------------+----------------+

-  measurements always come with an uncertainty. This uncertainty is
   depending on the particular measurement. Measured values shall always
   be stored with an uncertainty / error
-  every contribution that is added to the **LARA database** should be
   done with best knowledge and based on *Good Laboratory Practise*
-  direct accoutabilty and addressebilty of each scientist is very
   important, therefore scientists are no abstract entry in the **LARA
   database**, but all are part of the scientific community; their
   present and past affilitions shall be preserved for tracing back;
   each scientist receives a unique Identifier and data, she or he
   produced is directly related to her or him; literature and
   publications use the same base of information about the scientists as
   the projects and experiments

Synchronisation of data between different laboratories to exchange data.
------------------------------------------------------------------------

**The LARA database** tries to relate data as much as possible, applying
`Semantic Web <https://en.wikipedia.org/wiki/Semantic_Web>`__
technologies (*ontogies*, *OWL*, *RDF*, *SparQL*), this shall enable
modern **Machine Learing / AI** tools to access the **LARA database** in
a machine comprehensable and structured way, supplying these algorithms
with as much *meta data* about measurements and their relations as
possible.

Long term vision
----------------

Systems like the **LARA suite** shall in the long term *replace* the old
tradition of article based publication practice.

The LARA workflow
-----------------

The modules in the LARAsuite try cover all aspects of a common
laboratory workflow, starting from the planning of the experiments until
the final presentation of the results.

.. figure:: docs/images/lara/LARA_workflow.svg
   :alt: lara-workflow

   lara-workflow

The LARA structure
------------------

The LARAsuite is very mudular, enabling a flexible extension of the
concept. The modules are shown in the followoing graph:

.. figure:: docs/images/lara/LARA_structure.svg
   :alt: lara-structure

   lara-structure

The LARAsuite architecture
--------------------------

The current architecture of **LARA** has multiple layers as shown in the
following scheme: It is based on a relational database system (e.g.
postgreSQL), modules, written with `python-django <djangoproject.com>`__
access the database through the *django ORM*. A gRPC server connects to
these modules and can be used to execute remote procedure calls of a
modern web client (e.g. written with `vue.js <http://vuejs.org>`__ or
similar javascript frameworks).

.. figure:: docs/images/lara/LARA_application_architecture.svg
   :alt: LARA-architecture

   LARA-architecture

Components
----------

|Projects| `Projects <https://larasuite.gitlab.io/lara-django-projects>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

All labwork can be broken down into Projects and Experiments. The
Projects module supports in creating and managing these projects.

|Processes| `Processes <https://larasuite.gitlab.io/lara-django-processes>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

All processes can be created, managed and stored in the process module.

|Data| `Data <https://larasuite.gitlab.io/lara-django-data>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The raw and processed data can be stored in this module

|Substances| `Substances <https://larasuite.gitlab.io/lara-substances>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Chemical substances are stored and managed here

|Containers| `Containers <https://larasuite.gitlab.io/lara-django-containers>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The container management module

|Devices| `Devices <https://larasuite.gitlab.io/lara-django-devices>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Parts and Device Manager

|LabStore| `LabStore <https://larasuite.gitlab.io/lara-django-substances-store>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Keeping track of Substances and Parts

Ordering new things, when empty / used ...

|LaraUser| `User Management <https://larasuite.gitlab.io/lara-django-people>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Managing all related users

Library
~~~~~~~

Literature, Webpage and URL collections

Calendar
~~~~~~~~

Calendar for Users, Groups for Project / Experiment / Process planning
and Device booking

.. |LARA-logo| image:: docs/images/lara/LARA_logo.svg
.. |Projects| image:: docs/images/lara/LARA_projects_icon.png
.. |Processes| image:: docs/images/lara/LARA_process_icon.png
.. |Data| image:: docs/images/lara/LARA_data_icon.png
.. |Substances| image:: docs/images/lara/LARA_substances_icon.png
.. |Containers| image:: docs/images/lara/LARA_container_icon.png
.. |Devices| image:: docs/images/lara/LARA_devices_icon.png
.. |LabStore| image:: docs/images/lara/LARA_labstore_icon.png
.. |LaraUser| image:: docs/images/lara/LARA_people_icon.png

